package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;       
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.*;

@Path("/cars")
public class CarManager {
	
	private final static SortedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());                               

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] tmp = new Car[cList.size()];
		for(int i =0; i< cList.size(); i++) {
			tmp[i] = cList.get(i);
		}
		return tmp;
	}   
	

	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCar(@PathParam("id") long id){
		  for(int i = 0; i<cList.size();i++) {
			  if(cList.get(i).getCarId() == id) {
				  return cList.get(i);
			  }
		  }
		  
	      throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	    
	  }  
	  
	  	@POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCar(Car car){
	      cList.add(car);
	      return Response.status(201).build();
	    }  
	  	

	  	

	    @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCar(Car car){
	    	for(int i = 0; i<cList.size(); i++) {
	    		if(car.getCarId() == cList.get(i).getCarId()) {
	    			cList.remove(i);
	    			cList.add(car);
	    	        return Response.status(Response.Status.OK).build();

	    		}
	    	}
	        return Response.status(Response.Status.NOT_FOUND).build();      
	      }
	    
	    @DELETE
	    @Path("/{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){
	    	for(int i = 0; i<cList.size();i++) {
	    		if(id == cList.get(i).getCarId()) {
	    			cList.remove(i);
	    	        return Response.status(Response.Status.OK).build();

	    	}
	      }
	        return Response.status(Response.Status.NOT_FOUND).build();      
	    }  
	   
}
