package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import edu.uprm.cse.datastructures.cardealer.model.*;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E>  {

		DNode<E> header;
		private int size;
		private Comparator<E> comp;

	     
	    public CircularSortedDoublyLinkedList(Comparator<E> cmpr) { //CSDLL constructor with dummy header pointing to itself
	    	comp = cmpr;
	    	header = new DNode<E>();
	    	header.setNext(header);
	    	header.setPrev(header);
	        size = 0;
	    }
	 @Override
	public Iterator<E> iterator() {
//		 Iterator<E> it = new Iterator<E>() {

//	            private int currentIndex = 0;
//            	DNode<E> temp = header.getNext();
//
//	            @Override
//	            public boolean hasNext() {
//	                return currentIndex < size && temp.getElement() != null;
//	            }
//
//	            @Override
//	            public E next() {
//	            	currentIndex++;
//	            	temp = temp.getNext();
//	            	
//	                return temp.getElement();
//	            }
//
//	            @Override
//	            public void remove() {
//	                throw new UnsupportedOperationException();
//	            }
//	        };
	        return null;
	    }
	

	 
	@Override
	//Adds new car object to the list
	public boolean add(E obj) {
		DNode<E> nuevo = new DNode<E>();
		DNode<E> temp = header;
		nuevo.setElement(obj);
		if(isEmpty()) { //if empty, add new car to first node
			header.setNext(nuevo);
			header.setPrev(nuevo);
			nuevo.setNext(header);
			nuevo.setPrev(header);
		} else { //if not empty, start comparing first car to new car to sort
			temp = temp.getNext();
			

			while(temp != header) { //uses carComparator to see where new car needs to be placed
				if(comp.compare(nuevo.getElement(),temp.getElement()) > 0) {
					temp = temp.getNext();
			} else
				break;
		}
			nuevo.setNext(temp);
			nuevo.setPrev(temp.getPrev());
			temp.getPrev().setNext(nuevo);
			temp.setPrev(nuevo);
			
		}
		size++;
		return true;
	}

	
	@Override
	public int size() {
		
		return size;
	}

	@Override
	//removes car on the list that matches the parameter (car given by the user) and returns true if car is removed.
	//Otherwise, returns false
	public boolean remove(E obj) {
		if(isEmpty()) return false;
		DNode<E> temp = new DNode<E>();
		DNode<E> newPrev;
		DNode<E> newNext;
		temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(obj)) { //compares every car to the parameter car and removes if matches then ends method if removed
				newPrev = temp.getPrev();
				newNext = temp.getNext();
				newPrev.setNext(newNext);
				newNext.setPrev(newPrev);
				temp.cleanLinks();    //help garbage collector
				temp = null;
				size --;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	//removes car on the list that matches the parameter (position given by the user) and returns true if car is removed.
	//if index doesn't exist on list or list is empty, returns false
	public boolean remove(int index) {
		if(index>=size && isEmpty()) return false;
		int counter = 0;
		DNode<E> temp = new DNode<E>();
		DNode<E> newPrev;
		DNode<E> newNext;
		temp = header.getNext();
		while(counter!=index) {
			temp = temp.getNext();
			counter++;
		}
		newPrev = temp.getPrev();
		newNext = temp.getNext();
		newPrev.setNext(newNext);
		newNext.setPrev(newPrev);
		temp.cleanLinks();          //help garbage collector
		temp = null;
		size --;
		return true;
	}

	@Override
	//removes all cars on the list that matches the parameter (car given by the user) and returns the number of cars removed.
	public int removeAll(E obj) {
		if(isEmpty()) return 0;
		DNode<E> temp = new DNode<E>();
		DNode<E> newPrev;
		DNode<E> newNext;
		int numOfTimes = 0;
		temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(obj)) {
				numOfTimes++;
				newPrev = temp.getPrev();
				newNext = temp.getNext();
				newPrev.setNext(newNext);
				newNext.setPrev(newPrev);
				temp.cleanLinks();          //help garbage collector
				temp = null;
				temp = newPrev;
				size --;
			}
			temp = temp.getNext();
		}
		return numOfTimes;
	}

	@Override
	//returns the first car on the list
	public E first() {
		return header.getNext().getElement();
	}

	@Override
	//returns the last car on the list
	public E last() {
		return header.getPrev().getElement();
	}

	@Override
	//Returns the car on the position given as a parameter iterating through the list
	public E get(int index) {
		if(isEmpty()) return null;
		int counter = 0;
		DNode<E> temp = header.getNext();
		while(counter!=index) {
			temp = temp.getNext();
			counter++;
		}
		return temp.getElement();
	}

	@Override
	//deletes the list clearing all elements and nodes
	public void clear() {
		while (header != null) { 
			DNode<E> nnode = header.getNext(); 
			header.setElement(null); 
			header.cleanLinks(); 
			header = nnode; 
		}
	}

	@Override
	//returns true if the list contains the car given as a parameter and false otherwise
	public boolean contains(E e) {
		if(isEmpty()) return false;
		DNode<E> temp = new DNode<E>();
		temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return true;
			}
			temp = temp.getNext();
		}
		return false;	
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
		}

	@Override
	//returns the index/position of the first car on the list that matches the car given as a parameter
	//if car is not on the list, returns -1
	public int firstIndex(E e) {
		if(isEmpty()) return -1;
		DNode<E> temp = new DNode<E>();
		int indexCounter = 0;   //starts at the beginning of the list
		temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return indexCounter;
			}
			temp = temp.getNext();
			indexCounter++;
		}
		return -1;
	}

	@Override
	//returns the index/position of the last car on the list that matches the car given as a parameter
	//if car is not on the list, returns -1
	public int lastIndex(E e) {
		if(isEmpty()) return -1;
		DNode<E> temp = new DNode<E>();
		int indexCounter = size-1;   //starts at the end of the list
		temp = header.getPrev();
		while(temp.getPrev()!=header) {
			if(temp.getElement().equals(e)) {
				return indexCounter;
			}
			temp = temp.getPrev();
			indexCounter--;
		}
		return -1;
	}
	    
	//Double Node class for doubly linked list
	private static class DNode<E> implements Node<E> {
		private E element; 
		private DNode<E> prev, next; 

		// Constructors
		public DNode() {}
		
		public DNode(E e) { 
			element = e; 
		}
		
		public DNode(E e, DNode<E> p, DNode<E> n) { 
			prev = p; 
			next = n; 
		}
		
		// Methods
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public E getElement() {
			return element; 
		}

		public void setElement(E data) {
			element = data; 
		} 
		
		/**
		 * Just set references prev and next to null. Disconnect the node
		 * from the linked list.... 
		 */
		public void cleanLinks() { 
			prev = next = null; 
		}
		
	}
	
	

}
