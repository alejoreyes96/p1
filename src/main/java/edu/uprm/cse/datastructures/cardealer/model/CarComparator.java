package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;


public class CarComparator<E> implements Comparator<E> {
	
	@Override
	public int compare(E obj1, E obj2) {
		Car car1 = (Car) obj1;
		Car car2 = (Car) obj2;
		if(car1.getCarBrand().compareTo(car2.getCarBrand()) == 0) { 
			
			if(car1.getCarModel().compareTo(car2.getCarModel()) == 0) {
				
				//returns 0 if model, brand and option are equal
				//returns number < 0 if car1 should be placed before car2 on ascending list based on Model Option
				//returns number > 0 if car1 should be placed after car2 on ascending list based on Model Option
				return car1.getCarModelOption().compareTo(car2.getCarModelOption());
					
			} else 
				//returns number < 0 if car1 should be placed before car2 on ascending list based on Model
				//returns number > 0 if car1 should be placed after car2 on ascending list based on Model
				return car1.getCarModel().compareTo(car2.getCarModel());
		} else
			//returns number < 0 if car1 should be placed before car2 on ascending list based on Brand
			//returns number > 0 if car1 should be placed after car2 on ascending list based on Brand
			return car1.getCarBrand().compareTo(car2.getCarBrand());
	}

}
